#[tarpc::service]
pub trait RustMUD {
    async fn echo(input: String) -> String;
}
