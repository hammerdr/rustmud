use prost_build::Config;
use std::io::Result;
use std::path::PathBuf;

fn main() -> Result<()> {
    let mut config = Config::new();
    config.out_dir(PathBuf::from("../rustmud-gateway/src/proto/rustmud"));

    config.compile_protos(&["rustmud/gateway.proto"], &["rustmud/"])?;
    Ok(())
}
