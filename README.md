# rustmud

## Description
RustMUD is an implementation of a MUD server written in Rust lang. It's primarily used as a way to understand Rust. The TODO list can be found in the Roadmap section below. Please note that this is a toy.

## Installation
Follow instructions to install rustc from the website: https://www.rust-lang.org/learn/get-started

```
cd rustmud-api
cargo run
```

```
cd rustmud-gateway
cargo run
```

```
cd rustmud-app
npm start
```

## Usage
Once they are up, you should be able to open a browser and see communication happening back and forth.

## Packages

```
rustmud-api     => the API server that handles game logic
rustmud-app     => the React frontend for the game
rustmud-gateway => manages client connects and serves as go between of app and api
rustmud-proto   => protobuf definitions for app->gateway comms
rustmud-rpc     => rpc definitions for gateway->api comms
```

## Roadmap
- [x] Gateway Boot
- [x] Client Connect
- [x] Client Communication and Commands
- [x] Protobuf over Websocket
- [x] Server Boot
- [ ] Monorepo Startup
- [x] Gateway-Server RPC
- [ ] Account Setup
- [ ] Storage System
- [ ] Multiplayer Infrastructure

## License
See LICENSE for details
