/* eslint-disable */
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "rustmud";

export interface ConnectionOpen {
  version: string;
}

export interface ConnectionClose {
  reason: string;
}

export interface SimpleMessage {
  message: string;
}

export interface GatewayMessage {
  connectionOpen?: ConnectionOpen | undefined;
  connectionClose?: ConnectionClose | undefined;
  simpleMessage?: SimpleMessage | undefined;
}

function createBaseConnectionOpen(): ConnectionOpen {
  return { version: "" };
}

export const ConnectionOpen = {
  encode(message: ConnectionOpen, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.version !== "") {
      writer.uint32(10).string(message.version);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ConnectionOpen {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseConnectionOpen();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.version = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ConnectionOpen {
    return { version: isSet(object.version) ? String(object.version) : "" };
  },

  toJSON(message: ConnectionOpen): unknown {
    const obj: any = {};
    message.version !== undefined && (obj.version = message.version);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ConnectionOpen>, I>>(object: I): ConnectionOpen {
    const message = createBaseConnectionOpen();
    message.version = object.version ?? "";
    return message;
  },
};

function createBaseConnectionClose(): ConnectionClose {
  return { reason: "" };
}

export const ConnectionClose = {
  encode(message: ConnectionClose, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.reason !== "") {
      writer.uint32(10).string(message.reason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ConnectionClose {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseConnectionClose();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.reason = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ConnectionClose {
    return { reason: isSet(object.reason) ? String(object.reason) : "" };
  },

  toJSON(message: ConnectionClose): unknown {
    const obj: any = {};
    message.reason !== undefined && (obj.reason = message.reason);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ConnectionClose>, I>>(object: I): ConnectionClose {
    const message = createBaseConnectionClose();
    message.reason = object.reason ?? "";
    return message;
  },
};

function createBaseSimpleMessage(): SimpleMessage {
  return { message: "" };
}

export const SimpleMessage = {
  encode(message: SimpleMessage, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.message !== "") {
      writer.uint32(10).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SimpleMessage {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSimpleMessage();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SimpleMessage {
    return { message: isSet(object.message) ? String(object.message) : "" };
  },

  toJSON(message: SimpleMessage): unknown {
    const obj: any = {};
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<SimpleMessage>, I>>(object: I): SimpleMessage {
    const message = createBaseSimpleMessage();
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseGatewayMessage(): GatewayMessage {
  return { connectionOpen: undefined, connectionClose: undefined, simpleMessage: undefined };
}

export const GatewayMessage = {
  encode(message: GatewayMessage, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.connectionOpen !== undefined) {
      ConnectionOpen.encode(message.connectionOpen, writer.uint32(10).fork()).ldelim();
    }
    if (message.connectionClose !== undefined) {
      ConnectionClose.encode(message.connectionClose, writer.uint32(18).fork()).ldelim();
    }
    if (message.simpleMessage !== undefined) {
      SimpleMessage.encode(message.simpleMessage, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GatewayMessage {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGatewayMessage();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.connectionOpen = ConnectionOpen.decode(reader, reader.uint32());
          break;
        case 2:
          message.connectionClose = ConnectionClose.decode(reader, reader.uint32());
          break;
        case 3:
          message.simpleMessage = SimpleMessage.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GatewayMessage {
    return {
      connectionOpen: isSet(object.connectionOpen) ? ConnectionOpen.fromJSON(object.connectionOpen) : undefined,
      connectionClose: isSet(object.connectionClose) ? ConnectionClose.fromJSON(object.connectionClose) : undefined,
      simpleMessage: isSet(object.simpleMessage) ? SimpleMessage.fromJSON(object.simpleMessage) : undefined,
    };
  },

  toJSON(message: GatewayMessage): unknown {
    const obj: any = {};
    message.connectionOpen !== undefined &&
      (obj.connectionOpen = message.connectionOpen ? ConnectionOpen.toJSON(message.connectionOpen) : undefined);
    message.connectionClose !== undefined &&
      (obj.connectionClose = message.connectionClose ? ConnectionClose.toJSON(message.connectionClose) : undefined);
    message.simpleMessage !== undefined &&
      (obj.simpleMessage = message.simpleMessage ? SimpleMessage.toJSON(message.simpleMessage) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GatewayMessage>, I>>(object: I): GatewayMessage {
    const message = createBaseGatewayMessage();
    message.connectionOpen = (object.connectionOpen !== undefined && object.connectionOpen !== null)
      ? ConnectionOpen.fromPartial(object.connectionOpen)
      : undefined;
    message.connectionClose = (object.connectionClose !== undefined && object.connectionClose !== null)
      ? ConnectionClose.fromPartial(object.connectionClose)
      : undefined;
    message.simpleMessage = (object.simpleMessage !== undefined && object.simpleMessage !== null)
      ? SimpleMessage.fromPartial(object.simpleMessage)
      : undefined;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
