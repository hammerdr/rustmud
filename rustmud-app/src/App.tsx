import React from 'react';
import './App.css';
import {v4 as uuid} from 'uuid';
import {GatewayMessage} from './proto/rustmud/gateway';

interface WSData {
  id: string;
  message: string;
}

function useWebsocket() {
  const [data, setData] = React.useState<WSData[]>([]);
  const websocket = React.useMemo(() => {
    return new WebSocket("ws://192.168.1.38:8080");
  }, []);

  React.useEffect(() => {
    websocket.onopen = () => {
      websocket.send(GatewayMessage.encode({
        connectionOpen: { version: '1.0.0'},
      }).finish());
    };

    websocket.onmessage = function (event) {
      const {data} = event;
      data.arrayBuffer().then((rawData: ArrayBuffer) => {
        const {simpleMessage} = GatewayMessage.decode(new Uint8Array(rawData));
        console.log(event, data.arrayBuffer(), simpleMessage);
        if (simpleMessage == null) return;

        try {
          setData((currentData) => [
            ...currentData,
            {id: uuid(), message: simpleMessage.message}
          ]);
        } catch (err) {
          console.log(err);
        }
      });
    };

  }, [setData, websocket]);

  return {data, websocket};
}

function App() {
  const {data, websocket} = useWebsocket();

  const handleClick = React.useCallback(() => {
    websocket.send(GatewayMessage.encode({
      simpleMessage: {message: 'A new message'},
    }).finish());
  }, [websocket]);

  return (
    <div className="App">
      {data.map((datum) => <div key={datum.id}>{datum.message}</div>)}
      <div onClick={handleClick}>
        Click Me
      </div>
    </div>
  );
}

export default App;
