use std::{env, io::{Error, ErrorKind}, io::Cursor};
use std::net::{IpAddr, Ipv6Addr};

use futures_util::{StreamExt, TryStreamExt};
use log::info;
use tokio::net::{TcpListener, TcpStream};
use crate::proto::rustmud::rustmud::{GatewayMessage, SimpleMessage, gateway_message::MessageType};
use rustmud_rpc::RustMUDClient;
use tarpc::{client, context, tokio_serde::formats::Json};
use tokio_tungstenite::tungstenite::Message;
use prost::Message as ProstMessage;

pub mod proto;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let _ = env_logger::try_init();
    let addr = env::args().nth(1).unwrap_or_else(|| "0.0.0.0:8080".to_string());

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let listener = try_socket.expect("Failed to bind");
    info!("Listening on: {}", addr);

    while let Ok((stream, _)) = listener.accept().await {
        tokio::spawn(accept_connection(stream));
    }

    Ok(())
}

async fn build_client() -> Result<RustMUDClient, anyhow::Error> {
    let server_addr = (IpAddr::V6(Ipv6Addr::LOCALHOST), 3333);
    let transport = tarpc::serde_transport::tcp::connect(server_addr, Json::default);
    Ok(RustMUDClient::new(client::Config::default(), transport.await?).spawn())
}

async fn echo_message(client: RustMUDClient, message: String) -> anyhow::Result<Option<Message>> {
    Ok(Some(create_simple_message(client.echo(context::current(), message).await?)))
}

fn create_simple_message(message: String) -> Message {
    let mut buf = Vec::new();
    let mut simple_message = SimpleMessage::default();
    simple_message.message = message;
    let mut message = GatewayMessage::default();
    message.message_type = Some(MessageType::SimpleMessage(simple_message));

    buf.reserve(message.encoded_len());
    message.encode(&mut buf).unwrap();
    Message::from(buf)
}

async fn wrap_handle_message(message: Message) -> Result<Option<Message>, tokio_tungstenite::tungstenite::Error> {
    let result = handle_message(message).await;
    match result {
        Ok(msg) => Ok(msg),
        Err(msg) => Err(tokio_tungstenite::tungstenite::Error::Io(
            Error::new(ErrorKind::Other, format!("{}", msg))
        ))
    }
}

async fn handle_message(message: Message) -> anyhow::Result<Option<Message>> {
    let client = build_client().await?;
    match GatewayMessage::decode(&mut Cursor::new(message.into_data())) {
        Ok(message) => {
            match message.message_type {
                None => Ok(None),
                Some(MessageType::ConnectionClose(_)) => Ok(None),
                Some(MessageType::ConnectionOpen(_)) => {
                    Ok(Some(create_simple_message("Connected!".to_string())))
                },
                Some(MessageType::SimpleMessage(_)) => {
                    echo_message(client, "Simple message!".to_string()).await
                },
            }

        },
        Err(error) => Err(error.into())
    }
}

async fn accept_connection(stream: TcpStream) {
    let addr = stream.peer_addr().expect("connected streams should have a peer address");
    info!("Peer address: {}", addr);

    let ws_stream = tokio_tungstenite::accept_async(stream)
        .await
        .expect("Error during the websocket handshake occurred");

    info!("New WebSocket connection: {}", addr);

    let (write, read) = ws_stream.split();
    // We should not forward messages other than text or binary.
    read.try_filter_map(wrap_handle_message)
        .forward(write)
        .await
        .expect("Failed to forward messages")
}
