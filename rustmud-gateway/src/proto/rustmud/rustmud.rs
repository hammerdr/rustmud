#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ConnectionOpen {
    #[prost(string, tag = "1")]
    pub version: ::prost::alloc::string::String,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ConnectionClose {
    #[prost(string, tag = "1")]
    pub reason: ::prost::alloc::string::String,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SimpleMessage {
    #[prost(string, tag = "1")]
    pub message: ::prost::alloc::string::String,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GatewayMessage {
    #[prost(oneof = "gateway_message::MessageType", tags = "1, 2, 3")]
    pub message_type: ::core::option::Option<gateway_message::MessageType>,
}
/// Nested message and enum types in `GatewayMessage`.
pub mod gateway_message {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum MessageType {
        #[prost(message, tag = "1")]
        ConnectionOpen(super::ConnectionOpen),
        #[prost(message, tag = "2")]
        ConnectionClose(super::ConnectionClose),
        #[prost(message, tag = "3")]
        SimpleMessage(super::SimpleMessage),
    }
}
